// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCVw79YG5jYEPI5u-YbU2BRbTVOCiRA2E0",
    authDomain: "dream-games-9f0b9.firebaseapp.com",
    databaseURL: "https://dream-games-9f0b9.firebaseio.com",
    projectId: "dream-games-9f0b9",
    storageBucket: "dream-games-9f0b9.appspot.com",
    messagingSenderId: "47854720072",
    appId: "1:47854720072:web:7161a5219f98e77b8f747d",
    measurementId: "G-LZ43KEQC77"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCVw79YG5jYEPI5u-YbU2BRbTVOCiRA2E0",
    authDomain: "dream-games-9f0b9.firebaseapp.com",
    databaseURL: "https://dream-games-9f0b9.firebaseio.com",
    projectId: "dream-games-9f0b9",
    storageBucket: "dream-games-9f0b9.appspot.com",
    messagingSenderId: "47854720072",
    appId: "1:47854720072:web:7161a5219f98e77b8f747d",
    measurementId: "G-LZ43KEQC77"
  }
};

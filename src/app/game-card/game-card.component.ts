import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.sass']
})
export class GameCardComponent implements OnInit {

  @Input() game;

  constructor() { }

  ngOnInit(): void {
    console.log(this.game);
  }

}

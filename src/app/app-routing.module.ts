import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameListContainerComponent } from './game-list-container/game-list-container.component';

const routes: Routes = [
  { path: 'games', component: GameListContainerComponent },
  { path: '', component: GameListContainerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit, Input } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-game-list-container',
  templateUrl: './game-list-container.component.html',
  styleUrls: ['./game-list-container.component.sass']
})
export class GameListContainerComponent implements OnInit {
  private _games: any[] = [];
  public publicGames: any[];
  public counter: number;

  constructor(private _firebaseService: FirebaseService) {}

  ngOnInit(): void {
    this._getGames().then(res => {
      res.forEach(game => {
        this._games.push(game.payload.doc.EE.kt.proto.mapValue.fields);
      });
      this.publicGames = this._games;
      this._updateCounter();
    });
  }

  public searchByName(searchValue: string): void {
    let value = searchValue.toLowerCase();
    if (value) {
      this.publicGames = [];
      this._games.forEach(game => {
        if (game.name.stringValue.toLowerCase().indexOf(value) !== -1) {
          this.publicGames.push(game);
        }
      });
      this._updateCounter();
    } else {
      this.publicGames = this._games;
      this._updateCounter();
    }
  }

  private _getGames(): any {
    return this._firebaseService.getGames();
  }

  private _updateCounter(): void {
    this.counter = this.publicGames.length;
  }
}

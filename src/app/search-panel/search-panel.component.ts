import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.sass']
})
export class SearchPanelComponent implements OnInit {
  searchValue: string = "";
  @Output() onKeyUp = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  public search(): void {
    this.onKeyUp.emit(this.searchValue);
  }

}

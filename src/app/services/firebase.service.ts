import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(public db: AngularFirestore) { }

  public getGames(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.db.collection('games').snapshotChanges()
        .subscribe(snapshots => {
          resolve(snapshots)
        })
    })
  }

  public searchGames(searchValue: string) {
    return this.db.collection('games', ref => ref.where('name', '==', searchValue))
      .snapshotChanges();
  }
}
